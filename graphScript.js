
var root = {
	"label": "flare",
	"level": 1,
  "icon": "https://geology.com/google-earth/google-earth.jpg",
	"children": [{
    "label": "visual  analytics v3",
		"level": 2,
		"children": [{
				"label": "cluster",
				"level": 3,
				"children": [{
						"label": "AgglomerativeCluster",
						"size": 3938,
						"level": 4
					},
					{
						"label": "CommunityStructure",
						"size": 3812,
						"level": 4
					},
					{
						"label": "HierarchicalCluster",
						"size": 6714,
						"level": 4
					},
					{
						"label": "MergeEdge",
						"size": 743,
						"level": 4
					}
				]
			},
			{
				"label": "graph",
				"level": 3,
				"children": [{
						"label": "BetweennessCentrality",
						"size": 3534,
						"level": 4
					},
					{
						"label": "LinkDistance",
						"size": 5731,
						"level": 4
					},
					{
						"label": "MaxFlowMinCut",
						"size": 7840,
						"level": 4
					},
					{
						"label": "ShortestPaths",
						"size": 5914,
						"level": 4
					},
					{
						"label": "SpanningTree",
						"size": 3416,
						"level": 4
					}
				]
			},
			{
				"label": "optimization",
				"level": 3,
				"children": [{
					"label": "AspectRatioBanker",
					"size": 7074,
					"level": 4
				}]
			}
		]
	}]
};
var InitIsDone=false;

var width = 1000,
    height = 1000,
    root;

var force = d3.layout.force()
    .linkDistance(80)
    .charge(-120)
    .gravity(.05)
    .size([width, height])
    .on("tick", tick);

var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height)
    .call(d3.behavior.zoom().scaleExtent([1, 8])
    .on("zoom", zoom))
    .append('g');
    ;

var link = svg.selectAll(".link");

var node = svg.selectAll(".node")
    .data(force.nodes())
  .enter().append("g")
    .attr("class", "node")
    .on("mouseover", mouseover)
    .on("mouseout", mouseout);

    var rect = svg.append("rect")
    .attr("width", width)
    .attr("height", height)
    .style("fill", "none")
    .style("pointer-events", "all");

update();

d3.select("g").transition().each("end", function() 
{
if(!InitIsDone)
{
  InitIsDone=true;
   d3.selectAll('g.node')
    .each(function(d) {
      d3.select(this) 
      if(d.level != 3) return;
        d._children = d.children;
        d.children = null;
    });
    update();
}
});

function update() {
  var nodes = flatten(root),
      links = d3.layout.tree().links(nodes);

  // Restart the force layout.
  force
      .nodes(nodes)
      .links(links)
      .start();

  // Update links.
  link = link.data(links, function(d) { return d.target.id; });

  link.exit().remove();

  link.enter().insert("line", ".node")
      .attr("class", "link");

  // Update nodes.
  node = node.data(nodes, function(d) { return d.id; });

  node.exit().remove();

  var nodeEnter = node.enter().append("g")
      .attr("class", "node")
      .on("dblclick", click)
      .call(force.drag);

  nodeEnter.append("circle")
      .attr("r", function(d) { return Math.sqrt(d.size) / 10 || 4.5; });

 nodeEnter.append("image")
      .attr("xlink:href", function(d) { return d.icon; })
      .attr("x", "-12px")
      .attr("y", "-12px")
      .attr("width", "24px")
      .attr("height", "24px");

  nodeEnter.append("text")
      .attr("x", 12)
      .attr("dy", "-.88em")
      .text(function(d) { 
        return d.label
      });

  node.select("circle")
      .style("fill", color);
}

function tick() {
  link.attr("x1", function(d) { return d.source.x; })
      .attr("y1", function(d) { return d.source.y; })
      .attr("x2", function(d) { return d.target.x; })
      .attr("y2", function(d) { return d.target.y; });

  node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
}

function color(d) {
  return d._children ? "#3182bd" // collapsed package
      : d.children ? "#c6dbef" // expanded package
      : "#fd8d3c"; // leaf node
}

// Toggle children on click.
function click(d) {
  if (d3.event.defaultPrevented) return; // ignore drag
  if (d.children) {
    d._children = d.children;
    d.children = null;
  } else {
    d.children = d._children;
    d._children = null;
  }
  update();
}

// Returns a list of all nodes under the root.
function flatten(root) {
  var nodes = [], i = 0;

  function recurse(node) {
    if (node.children) node.children.forEach(recurse);
    if (!node.id) node.id = ++i;
    nodes.push(node);
  }

  recurse(root);
  return nodes;
}

function mouseover() {
  d3.select(this).select("circle").transition()
      .duration(750)
      .attr("r", 16);
}

function mouseout() {
  d3.select(this).select("circle").transition()
      .duration(750)
      .attr("r", 8);
}

function zoom() {
  svg.attr("transform", "translate(" + d3.event.translate + ")" + " scale(" + (d3.event.scale) + ")");
}